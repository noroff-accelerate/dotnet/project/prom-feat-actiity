# Virtual Book Club Activity Sample Solution

This repository contains a sample solution for a virtual book club activity using C# and .NET. The solution consists of a simple program that manages a collection of book club members and filters them based on their favorite genres. 

## Table of Contents
- Introduction
- Prerequisites
- Getting Started
- Usage
- License

## Introduction
The sample solution includes the following components:
- `Program`: The main entry point of the application that demonstrates the usage of the repository and filtering by favorite genre.
- `Member`: A class representing a book club member with properties such as Name, JoinedDate, and FavoriteGenres.
- `Repository<T>`: A generic repository class for storing and managing collections of items.
- `RepositoryExtensions`: An extension class that adds a filtering method to the repository to filter members by their favorite genre.

## Prerequisites
To run this sample solution, you need to have the following prerequisites installed on your machine:
- .NET SDK

## Getting Started
1. Clone this repository to your local machine.
2. Navigate to the project directory.
3. Open the project in your preferred code editor or IDE.

## Usage
To use this sample solution, you can follow these steps:

1. In the `Program.cs` file, customize the creation of book club members by adding more members to the `membersRepository` using the `Add` method.

2. Modify the favorite genres and member details as needed in the `Member` class.

3. To filter members by their favorite genre, use the `FilterByFavoriteGenre` method on the `membersRepository`.

4. Run the program to see the filtered results and display the names of members who love the specified genre.


## License
This sample solution is provided under the MIT License. You are free to use and modify it for your own purposes.
