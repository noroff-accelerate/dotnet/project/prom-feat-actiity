﻿namespace Noroff.Samples.PromFeatActivity
{
    public class Program
    {
        static void Main(string[] args)
        {
            var membersRepository = new Repository<Member>();
            membersRepository.Add(new Member("Alice", new DateTime(2020, 1, 1), new List<string> { "Fantasy", "Science Fiction" }));
            membersRepository.Add(new Member("Bob", new DateTime(2019, 5, 23), new List<string> { "Mystery", "Thriller" }));

            var fantasyLovers = membersRepository.FilterByFavoriteGenre("Fantasy");

            Console.WriteLine("Fantasy Genre Lovers:");
            foreach (var member in fantasyLovers)
            {
                Console.WriteLine(member.Name);
            }
        }
    }


    public class Member
    {
        public string Name { get; set; }
        public DateTime JoinedDate { get; set; }
        public List<string> FavoriteGenres { get; set; }

        public Member(string name, DateTime joinedDate, List<string> favoriteGenres)
        {
            Name = name;
            JoinedDate = joinedDate;
            FavoriteGenres = favoriteGenres;
        }
    }

    public class Repository<T>
    {
        private List<T> items = new List<T>();

        public void Add(T item)
        {
            items.Add(item);
        }

        public IEnumerable<T> GetAll()
        {
            return items;
        }
    }

    public static class RepositoryExtensions
    {
        public static IEnumerable<Member> FilterByFavoriteGenre(this Repository<Member> repository, string genre)
        {
            foreach (var member in repository.GetAll())
            {
                if (member.FavoriteGenres.Contains(genre))
                {
                    yield return member;
                }
            }
        }
    }
}